<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.users.add_update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'email' => 'required|unique:users,email',
        ]);
        
        try {
          User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password)
          ]);
          return redirect('/admin/users')->with('message', 'User successfully added.');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('*');
            return Datatables::of($data)

                ->addColumn('action', function($row){
                    $actionBtn = '<a href="' . route('admin.edituser', encrypt($row->id)) . '" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" onclick="deleteUser('.$row->id.')" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id = decrypt($id);
        // dd($id);
        $data = User::where('id',$id)->first();
       
        return view('admin.users.add_update')->with(['data' => $data]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users,email,' . $request->edit_id
        ]);
        
        try {

            if($request->password == ''){
                $data       = User::select('password')->where('id',$request->edit_id)->first();
                $password   = $data->password;
            }else{
                $password   = Hash::make($request->password);
            }

          User::where('id',$request->edit_id)->update([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => $password,
          ]);
          return redirect('/admin/users')->with('message', 'User successfully updated.');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        try {
            $data = User::findOrFail($request->id);
            $data->delete();
           return response()->json('success');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
