<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Product;
use File;
use DataTables;

class ProductControlle extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.products.add_update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $name        = $request->name;
            $description = $request->description;
            $price       = $request->price;
            $quantity    = $request->quantity;
            $product_img='';
            if ($request->hasFile('pro_img')) {
                $product_img = $request->pro_img->getClientOriginalExtension();
                $product_img = time().'.'.$product_img; // Add current time before image name
                $request->pro_img->move(public_path().'/images/product/',$product_img);
                $product_img = $product_img;
            }

            $id = Product::create([
                'name'          =>  $name,
                'description'   =>  $description,
                'price'         =>  $price,
                'pro_image'     =>  $product_img,
                'quantity'      =>  $quantity,
            ]);
          return redirect('/admin/products')->with('message', 'Product successfully added.');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::select('*');
            return Datatables::of($data)
            ->editColumn('pro_image', function ($row) {
                $file = $row->pro_image;
                if($file == ''){
                    return asset('/images/img')."/image-placeholder.png";
                }else{
                    return asset('/images/product')."/".$file;
                }
            })
            ->addColumn('action', function($row){
                $actionBtn = '<a href="' . route('admin.editproduct', encrypt($row->id)) . '" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" onclick="deleteProduct('.$row->id.')" class="delete btn btn-danger btn-sm">Delete</a>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $data = Product::where('id',$id)->first();
        return view('admin.products.add_update')->with(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        try {
        
            $edit_id        = $request->edit_id;
            $name        = $request->name;
            $description = $request->description;
            $price       = $request->price;
            $quantity    = $request->quantity;
            $pro_img='';
            if($request->hasFile('pro_img') && $request->pro_img)
            {   
              $oldImageurl = public_path("/images/product/{$request->hiddenpro_img}"); // get previous image from folder

              if (isset($request->hiddenpro_img) && $request->hiddenpro_img !='' &&  File::exists($oldImageurl)) { // unlink or remove previous image from folder
                  unlink($oldImageurl);
              }

              $pro_img = time() . '.' .
                $request->pro_img->getClientOriginalExtension();
              $request->file('pro_img')->move(public_path().'/images/product/', $pro_img);
            }
            if(isset($pro_img) && $pro_img != ''){
                $pro_img = $pro_img;
            }else{
                $pro_img = $request->hiddenpro_img;
            }
            $id = Product::where('id',$edit_id)->update([
                    'name'          =>  $name,
                    'description'   =>  $description,
                    'price'         =>  $price,
                    'pro_image'     =>  $pro_img,
                    'quantity'      =>  $quantity,
                ]);
            return redirect('/admin/products')->with('message', 'Product successfully updated.');
        } catch (\Exception $e) {

            return $e->getMessage();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
         try {
            $data = Product::findOrFail($request->id);
            $data->delete();
           return response()->json('success');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
