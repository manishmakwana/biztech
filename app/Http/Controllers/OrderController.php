<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Order;
use Auth;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $products = Product::get();
            return view('productlist')->with('products',$products);
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function itemAddToCart(Request $request)
    {
        try {
            if(Auth::guard('web')->check())
                $id = Auth::guard('web')->user()->id;
            else{
                $id = '';
            }
            
            $user_id    = $id;
            $product_id = $request->product_id;
            $select_qnt = $request->select_qnt;
            $total_price= $request->total_price;
            $totalqnt   = $request->totalqnt;

            $id = Order::create([
                'user_id'       =>  $user_id,
                'product_id'    =>  $product_id,
                'product_qty'   =>  $select_qnt,
                'total_price'   =>  $total_price
            ])->id;

            $totalqnt = $totalqnt-$select_qnt;
            Product::where('id',$product_id)->update([
                    'quantity'      =>  $totalqnt,
                ]);
          return response()->json(['id' => $id, 'totalqnt' => $totalqnt, 'login_id' => $user_id]); 
        } catch (\Exception $e) {

            return $e->getMessage();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getByNowProduct(Request $request)
    {

        try{
            
            if(Auth::guard('web')->check())
                $id = Auth::guard('web')->user()->id;
            else{
                $id = '';
            }
            // $orders = Order::where('user_id',$id)->get();
            $orders = DB::table('orders')
                ->select('orders.id as order_id','orders.product_id','orders.total_price','orders.product_qty','products.name')
                ->leftJoin('products','products.id','=','orders.product_id')
                ->where(['orders.user_id' => $id])
                ->get();
            return view('cartlist')->with('orders',$orders);
        } catch (\Exception $e) {

            return $e->getMessage();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function penddingProductOrder(Request $request)
    {
        // dd($request->arrcart);
        $arrcart = $request->arrcart;
        if(count($arrcart) > 0){
            if(Auth::guard('web')->check())
                $id = Auth::guard('web')->user()->id;
            else{
                $id = '';
            }

            foreach ($arrcart as $key => $c) {
                Order::where('id',$c)->update([
                    'user_id'      => $id,
                ]);
            }
            return response()->json(['msg' => 'success' ]);
        }
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCartItem(Request $request)
    {
        try{
            
            if(Auth::guard('web')->check())
                $id = Auth::guard('web')->user()->id;
            else{
                $id = '';
            }

            $totalqnt = Product::where('id',$request->product_id)->first();

            $totalqnt = $totalqnt->quantity + $request->qty;
            
            Product::where('id',$request->product_id)->update([
                    'quantity'      =>  $totalqnt,
                ]);

            Order::where('id',$request->orderid)->delete();
            
            return response()->json(['msg' => 'success' ]);

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
