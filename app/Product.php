<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
   	protected $table = 'products';
   	/**
     * The primary key associated with the table.
     *
     * @var string
     */
    // protected $primaryKey = 'id';


    protected $fillable = [
                'name',
                'description',
                'price',
                'pro_image',
                'quantity'
            ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
}
