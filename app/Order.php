<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
   	protected $table = 'orders';
   	/**
     * The primary key associated with the table.
     *
     * @var string
     */
    // protected $primaryKey = 'id';


    protected $fillable = [
                'user_id',
                'product_id',
                'product_qty',
                'total_price',
            ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
}
