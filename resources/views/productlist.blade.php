@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Products') }}</div>
                <div class="card-body">
                   <div class="row">
                    <?php
                        if(count($products) > 0){
                            foreach ($products as $key => $p) {
                                if ($p->pro_image != '') {
                                    $imagesrc = asset('images/product').'/'.$p->pro_image; 
                                }else{
                                    $imagesrc = asset('images/img/image-placeholder.png'); 
                                }
                                echo '<div class="col-sm-3 text-center"><img src="'.$imagesrc.'" width="200" height = "200">
                                <div><h6>'.$p->name.'</h6><p>'.$p->description.'</p><p id="pro_total_qty" pro_total_qty="'.$p->quantity.'"></p></div><div>';
                                if ($p->quantity > 0) {
                                    echo '<div>In stack</div><p id="price_'.$p->id.'" price="'.$p->price.'">$'.$p->price.'</p> <input type="text" name="quantity" id="selectitem_'.$p->id.'" value="1" size="2" class="input-cart-quantity"><a href="javascript:void(0)" onclick="addToCart('.$p->id.','.$p->quantity.')"class="btn btn-info btn-sm"><img src="'.asset("images/img/cart.png").'" width="20" height = "20"></a></div>
                                    <span class="error_'.$p->id.'" style="color:red"></span></div>';
                                }else{
                                     echo '<div>Out of stack</div>$'.$p->price.'<input type="text" name="quantity" value="0" size="2" class="input-cart-quantity" read readonly><a href="javascript:void(0)"  class="btn btn-secondary btn-sm"><img src="'.asset("images/img/cart.png").'" width="20" height = "20"></a></div></div>';
                                }
                               
                            }
                        }else{

                            echo '<div class="col-sm-12 text-center">Record Not found.</div>';
                        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
    $('.input-cart-quantity').keypress(function(event){

       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
       }

   });

});
function addToCart(id,totalqnt) {
    var select_qnt = $('#selectitem_'+id).val();
    var select_price = $('#price_'+id).attr('price');
    if(totalqnt < select_qnt){
        $('span.error_'+id).html('please select less than '+totalqnt+' Quantity.');
    }else{
        total_price = select_price * select_qnt;

        $.ajax({
            type:'POST',
            url: "{{route('itemaddtocart')}}",
            dataType:'json',
            data: {
              _token:'{{ csrf_token() }}', 
              product_id    :id,
              select_qnt    :select_qnt,
              total_price   :total_price,
              totalqnt      :totalqnt
             },
            success:function(data){
                var cartListJson = localStorage.getItem('itemQuantity');
                var itemQuantity = cartListJson ? JSON.parse(cartListJson) : [];
                itemQuantity.push(data.id);
                localStorage.setItem('itemQuantity', JSON.stringify(itemQuantity));
                // $('#pro_total_qty').html('Total Quantity: '+data.totalqnt)
                location.reload();
            },
        });
    }
}
</script>
@endpush