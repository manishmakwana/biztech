<div id="copyright text-right">© Copyright 2021 BizTech </div>

 <script src="{{asset('js/jquery.min.js')}}"></script>
 
 <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
 <!-- <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script> -->
 <script src="{{asset('js/datatables.min.js')}}"></script>
 <script src="{{asset('js/jquery.validate.min.js')}}"></script>
 <script src="{{asset('js/additional-methods.min.js')}}"></script>
  @stack('scripts')