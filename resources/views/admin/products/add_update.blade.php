@extends('admin.layout.auth')

@section('content')
<?php

if(isset($data->id) && $data->id !=''){
  $id           = $data->id;
  $name         = $data->name;
  $description  = $data->description;
  $pro_img      = $data->pro_image;
  $price        = $data->price;
  $quantity     = $data->quantity;
}else{
  $id           = '0';
  $name        = '';
  $description  = '';
  $pro_img      = '';
  $price        = '';
  $quantity     = '';
}

  if($pro_img!= ''){
    $pro_img =  asset('/images/product')."/".$pro_img;
  }else{
    $pro_img = asset('/images/img')."/image-placeholder.png";
  }

?>
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Product</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form method="POST" action="{{isset($id) && $id != 0 ? route('admin.updateproduct') :route('admin.storeproduct')}}" id="product-form"  enctype="multipart/form-data" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="edit_id" value="{{isset($id)?$id:'0'}}">
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 form-group">
          <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name" value="{{$name}}" placeholder="Enter Name">
        </div>
        <div class="col-md-6 form-group">
          <label for="price">Price</label>
        <input type="text" name="price" class="form-control" id="price" value="{{$price}}" placeholder="Enter Title">
        </div>
       
      </div>
      <div class="row">
        <div class="col-md-6 form-group">
          <label for="description">Description</label>
          <textarea id="description" name="description" placeholder="Description" class="form-control" rows="4" cols="50">{{$description}}</textarea>
        </div>
        <div class="col-md-6 form-group">
        <label for="pro_img">Image</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="pro_img" id="pro_img">
            <input type="hidden" class="custom-file-input" value="{{(isset($data->pro_image) && $data->pro_image != '' ? $data->pro_image : '')}}" name="hiddenpro_img" id="hiddenpro_img">
            <label class="custom-file-label" for="pro_img">Choose file</label>
          </div>
          <img id="img_prview" src="{{$pro_img}}" alt="your image" height="100" width="100" />
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 form-group">
          <label for="quantity">Quantity</label>

          <input type="text" class="form-control datepicker" value="{{$quantity}}" placeholder="Quantity" name="quantity" id="quantity">
            
        </div>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#img_prview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#pro_img").change(function() {
    readURL(this);
  });

  $(document).ready(function () {
    $('#product-form').validate({ // initialize the plugin
        rules: {
            name: {
              required: true
            },
            price: {
              required: true
            },
            description: {
                required: true,                
            },
             quantity: {
              required: true,
              digits: true               
            },
            pro_img: {
              // required: true,
              extension: "jpg|jpeg|png"
            }
        }
    });
     $('#price').keypress(function (event) {
            return isNumber(event, this)
        });
  });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

      var charCode = (evt.which) ? evt.which : event.keyCode

      if (
          (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
          (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
          (charCode < 48 || charCode > 57))
          return false;

      return true;
    }  
</script>
@endpush