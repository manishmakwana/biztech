@extends('admin.layout.auth')

@section('content')
<div class="container">
    <div class="row">
    	<div class="session-msg">
	    	@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
    	</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"  >
                	<h2 class="pb-2">Product List <a href="{{route('admin.createproduct')}}" class="btn btn-info" style="float: right; ">Add Product</a></h2>
                </div>
                <div class="panel-body">
                    <table id="productlist-tbl" class="table table-bordered yajra-datatable">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>Name</th>
				                <th>Image</th>
				                <th>Price ($)</th>
				                <th>Quantity</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function () {
	iniProductlist();
});
	function iniProductlist() {
		var table = $('#productlist-tbl').DataTable({
        dom: 'lBfrtip',
        "destroy"   : true,
        "processing": true,
        "serverSide": true,
        "searching": true,
        "pageLength": 10,
        "order": [[ 0, "desc" ]],
        ajax: "{{ route('admin.getproductslist') }}",
	        columns: [
	            {data: 'id', name: 'id', "visible": false, searchable: false},
	            {data: 'name', name: 'name'},
	            { "render": 
		              function (data, type, JsonResultRow, meta) 
		              {
		                return '<img src="'+JsonResultRow.pro_image+'" border="0" width="50" height="50" class="img-rounded" align="center">';
		              }, searchable: false
		        },
	            {data: 'price', name: 'price'},
	            {data: 'quantity', name: 'quantity'},
	            {data: 'action',name: 'action',orderable: false,searchable: false},
	        ]
	    });
	}
	function deleteProduct(id) {
		if (confirm("Are you sure delete this Product?")) {
	        $.ajax({
            type:'POST',
            url: "{{route('admin.deleteproduct')}}",
            dataType:'json',
	            data: {
	              _token:'{{ csrf_token() }}', 
	              id:id
	             },
	            success:function(data){
	            	$('.session-msg').html('<div class="alert alert-success">User successfully Deleted.</div>');
	            	$('#productlist-tbl').DataTable().ajax.reload()
	            },
	        });
	    }
        
    }
</script>
@endpush
