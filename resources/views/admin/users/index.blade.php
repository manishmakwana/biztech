@extends('admin.layout.auth')

@section('content')
<div class="container">
    <div class="row">
    	<div class="session-msg">
	    	@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
    	</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"  >
                	<h2 class="pb-2">User List <a href="{{route('admin.createuser')}}" class="btn btn-info" style="float: right; ">Add User</a></h2>
                	
                </div>
                <div class="panel-body">
                    <table id="userlist-tbl" class="table table-bordered yajra-datatable">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>Name</th>
				                <th>Email</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function () {
	iniUserlist();

});
	function iniUserlist() {
		var table = $('#userlist-tbl').DataTable({
        processing: true,
        serverSide: true,
        "order": [[ 0, "desc" ]],
        ajax: "{{ route('admin.userlist') }}",
	        columns: [
	            {data: 'id', name: 'id', "visible": false, searchable: true},
	            {data: 'name', name: 'name'},
	            {data: 'email', name: 'email'},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: true, 
	                searchable: true
	            },
	        ]
	    });
	}
	function deleteUser(id) {
		if (confirm("Are you sure delete this user?")) {
	        $.ajax({
            type:'POST',
            url: "{{route('admin.deleteuser')}}",
            dataType:'json',
	            data: {
	              _token:'{{ csrf_token() }}', 
	              id:id
	             },
	            success:function(data){
	            	$('.session-msg').html('<div class="alert alert-success">User successfully Deleted.</div>');
	            	$('#userlist-tbl').DataTable().ajax.reload()
	            },
	        });
	    }
        
    }
</script>
@endpush
