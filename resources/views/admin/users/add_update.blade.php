@extends('admin.layout.auth')

@section('content')
<?php
  if(isset($data) && $data != ''){
    $id                 = $data->id;
    $name               = $data->name;
    $email              = $data->email;
  }else{
    $id                 = '0';
    $name               = old('name');
    $email              = old('email');    
  }
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" id="user-form" action="{{isset($id) && $id != 0 ? route('admin.updateuser') :route('admin.storeuser')}}">
                        @csrf
                        <input type="hidden" name="edit_id" value="{{isset($id)?$id:'0'}}">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input id="name" type="text" class="form-control" name="name" value="{{$name}}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" name="email" value="{{$email}}">
                           @if($errors->has('email'))
                                <label  class="error">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                             <input id="password" type="password" class="form-control" name="password" >
                            
                        </div>
                        <div class="form-group">
                            <label for="password-confirm">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {
        var id = '{{$id}}';

        if(id != 0){
            var pass_rule = false;
        }else{
            var pass_rule = true;
        }

        $("#user-form").validate({
            rules: {
              name: "required",
              email: {
                required: true,
                email: true
              },
              password: {
                required: pass_rule,
                minlength: 6
              },
              password_confirmation: {
                    required: pass_rule,
                    minlength: 6,
                    equalTo: "#password"
              }
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });
    
</script>
@endpush