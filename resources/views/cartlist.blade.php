@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('order list') }}</div>
                <div class="card-body">
                   <div class="row">
                    <table class="table">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col">Product name</th>
                          <th scope="col">Quantity</th>
                          <th scope="col">Price</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                            if(count($orders) > 0){
                                foreach ($orders as $key => $v) {
                                    echo "<tr>";
                                    echo "<td>".$v->name."</td>";
                                    echo "<td>".$v->product_qty."</td>";
                                    echo "<td>".$v->total_price."</td>";
                                    echo '<td><a href="javascript:void(0)" onclick="deleteCart('.$v->order_id.','.$v->product_id.','.$v->product_qty.')" class="btn btn-info btn-sm">Delete</a></td>';
                                    echo "</tr>";
                                }
                            }
                        ?>
                       
                      </tbody>
                    </table>
                    <a href="javascript:void(0)" class="delete btn btn-info btn-sm">Buy Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
    $('.input-cart-quantity').keypress(function(event){
       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
       }

   });

});
function deleteCart(orderid,productid,qty) {
  $.ajax({
        type:'POST',
        url: "{{route('deletecartitem')}}",
        dataType:'json',
        data: {
          _token:'{{ csrf_token() }}',
          orderid       :orderid,
          product_id    :productid,
          qty           :qty
         },
        success:function(data){
            location.reload();
        },
    });
}
</script>
@endpush