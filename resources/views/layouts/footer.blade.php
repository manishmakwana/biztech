<div id="copyright text-right">© Copyright 2021 BizTech </div>

 <script src="{{asset('js/jquery.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
  @stack('scripts')
  <script type="text/javascript">
  	
    $( document ).ready(function() {
    	<?php
    		if(Auth::guard('web')->check())
                $id = Auth::guard('web')->user()->id;
            else{
                $id = '';
            }
    	?>
    		var loginid = '{{$id}}';
	  
    		if (localStorage.getItem('itemQuantity') !== null) {
	        $('.order-cart span').html(JSON.parse(localStorage.getItem('itemQuantity')).length);
	        $('#mycartdata').val($('#mycartdata span').text() + localStorage.getItem('itemQuantity'));
	        arrcart = JSON.parse(localStorage.getItem('itemQuantity'));
	        if(loginid != ''){
		        $.ajax({
		            type:'POST',
		            url: "{{route('penddingproductorder')}}",
		            dataType:'json',
		            data: {
		              _token:'{{ csrf_token() }}', 
		              arrcart 		:arrcart	              
		             },
		            success:function(data){
		                var cartListJson = localStorage.removeItem('itemQuantity');
		                location.reload();
		            },
		        });
	        }
		  
    	}
	   	

	});
  </script>