# Laravel Practical Test

## Requirements

- Git
- PHP >= 7.2.5
- SQL Database (Mysql or postgres)
- [Composer](https://getcomposer.org/)

#### Required PHP Extensions

- BCMath
- Ctype
- Fileinfo
- JSON
- Mbstring
- OpenSSL
- PDO
- Tokenizer
- XML

**Note:**
Make sure you've generated SSH key(if not exists) and added it into your gitlab account. See [Create and add your SSH key pair](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html).

## What you have to do?

Create a web bases application which covers below features:

Admin panel with:
- Login
- User module
- Products module
- Report for the product sales on daily and user wise

Front end user:
- Registration
- Login
- Product listing like E-commerce site
- Place order
- If product quantity is less than zero(0), show out of stock status and disable "Buy Now" button

## What we're looking for
- **Use standard best practices when writing the code.** Code must be with proper standards, any standard you want just stick with it and specify it `README.md` file that which standard you used while writing your code.
- **Structure your code.** Structure your code properly into modularized way.
- **Code must be re-usable.** Split your code into small, re-usable components so that it can be easily reused in the future.
- **Focus of code readability.** While writing code have one thing in mind that this code not only read by you but also others, so code that way that it easier to grasp and understand by new developer. Add comments, use meaningful variable names, etc. do whatever necessary to achieve this.

## What we're not looking for
- **Don't worry about the design.** Don't give your time on the designing the pages, just structure the page properly so page doesn't look too bad to see.
- **You don't need to go crazy in terms of componentization.** Extract components when it helps you maintain what would otherwise be annoying duplication, but don't break things apart needlessly.
- **You don't need to write tests.** If they help you then sure go for it, but don't prioritize them.

## Start by:

- Forking this repo. Refer [this link](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) if you have no idea about how to fork a repo.
- [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) your forked repo into your system. And install project dependencies. Start doing your work. Along with your actual code, put together a short `README.md` file describing steps that we have to perform in order to get this project up and running.
- When you're done, create a Merge Request in this repo from your forked repo. Refer [this link](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) if you have no idea about how to submit a merge request.

And you're done! From there we'll take care of it and review your submission.

Thank you & best of luck.
