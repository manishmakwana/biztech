<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect()->route('productlist');
});

Route::get('/', 'OrderController@index')->name('productlist');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/itemaddtocart', 'OrderController@itemAddToCart')->name('itemaddtocart');

Route::post('/deletecartitem', 'OrderController@deleteCartItem')->name('deletecartitem')->middleware('auth:web');

Route::get('/admin', function () {
    return redirect()->route('admin.login');
});
Route::post('/bynowproduct', 'OrderController@getByNowProduct')->name('bynowproduct')->middleware('auth:web');
Route::post('/penddingproductorder', 'OrderController@penddingProductOrder')->name('penddingproductorder');

Route::group(['prefix' => 'admin','as'=>'admin.'], function () {
  Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'Admin\Auth\LoginController@login');
  Route::post('/logout', 'Admin\Auth\LoginController@logout')->name('logout');

  Route::get('/register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'Admin\Auth\RegisterController@register');

  Route::post('/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm');
  Route::group(['middleware'=>'admin'], function () {
    Route::get('/users', 'Admin\UserController@index');  
    Route::get('/getuserlist', 'Admin\UserController@show')->name('userlist');  
    Route::get('/createuser', 'Admin\UserController@create')->name('createuser'); 
    Route::post('/storeuser', 'Admin\UserController@store')->name('storeuser');  
    Route::get('/edituser/{id}', 'Admin\UserController@edit')->name('edituser');
    Route::post('/updateuser', 'Admin\UserController@update')->name('updateuser');
    Route::post('/deleteuser', 'Admin\UserController@destroy')->name('deleteuser'); 
      
    Route::get('/products', 'Admin\ProductControlle@index');  
    Route::get('/getproductslist', 'Admin\ProductControlle@show')->name('getproductslist');  
    Route::get('/createproduct', 'Admin\ProductControlle@create')->name('createproduct');
    Route::post('/storeproduct', 'Admin\ProductControlle@store')->name('storeproduct'); 
    Route::get('/editproduct/{id}', 'Admin\ProductControlle@edit')->name('editproduct');
    Route::post('/updateproduct', 'Admin\ProductControlle@update')->name('updateproduct');
    Route::post('/deleteproduct', 'Admin\ProductControlle@destroy')->name('deleteproduct');

  });
});



